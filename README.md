PyTiA server instance configured to send relative angle between a pair of arm
mounted IMUs, based on the RTIMULib2 sensor fusion algorithm.
