import RTIMU
import time, sys, itertools, math

have_pygame = True
try:
    import pygame
    from pygame.locals import *
except:
    have_pygame = False

from pyshake import *
from imulib import Connection, IMU
import config
import pytia
from pytia import TiAServer, TiAConnectionHandler, TiASignalConfig

class ScrollingGraph:
    def __init__(self, _bgcolor, _scolors, _name, _rect, _xpoints, _min, _max, _numstreams, yticks):
        self.bgcolor = _bgcolor
        self.stream_colors = _scolors
        self.name = _name
        self.rect = _rect
        self.xpoints = _xpoints
        self.min = _min
        self.max = _max
        self.numstreams = _numstreams
        self.font = pygame.font.Font(None, 20)
        self.fontsurf = self.font.render(_name, 1, (255, 255, 255))

        self.xstep = self.rect.width / (self.xpoints * 1.0)

        self.yscale = self.rect.height / (1.0 * (_max - _min))
        self.yoffset = 0
        if self.min < 0:
            self.yoffset = abs(self.min)

        tmp = [self.scale((abs(_max) - abs(_min)) / 2) for s in range(_numstreams)]
        self.data = [tmp for x in range(_xpoints)]
        self.yticks = [self.scale(yt) for yt in yticks]
        self.ylabels = map(int, yticks)
        self.tick_color = pygame.Color(125, 125, 125)

    def scale(self, val):
        # get value into 0-n range
        nv = val + self.yoffset

        # scale
        nv *= self.yscale

        # adjust
        nv = self.rect.height - nv

        # vertical offset for display rectangle
        nv += self.rect.y

        return nv

    def draw(self, surface, streamvals):
        surface.fill(self.bgcolor, self.rect)

        self.data = self.data[1:]

        adjvals = []
        for s in streamvals:
            adjvals.append(self.scale(s))
        self.data.append(adjvals)

        pointlist = [[] for x in range(self.numstreams)]
        for d in range(len(self.data)):
            for s in range(len(self.data[d])):
                y = self.data[d][s]
                pointlist[s].append((self.xstep * d, y))

        for i, yt in enumerate(self.yticks):
            tmp = self.font.render(str(self.ylabels[i]), 1, tick_color)
            surface.blit(tmp, (self.rect.w / 2, yt))
            pygame.draw.line(surface, tick_color, (0, yt), (self.rect.x + self.rect.w, yt))

        for s in range(self.numstreams):
            pygame.draw.lines(surface, self.stream_colors[s], False, pointlist[s], 1)
        surface.blit(self.fontsurf, (self.rect.x + 10, self.rect.y + 10))


def r2d(x):
    return (180 * x) / math.pi

def d2r(x):
    return (x / 180.0) * math.pi

class Test(object):

    def __init__(self):
        connected = False

        if isinstance(config.serial_port, list):
            for addr in config.serial_port:
                self.sk7 = Connection(addr)
                if self.sk7.connect():
                    connected = True
                    break
        else:
            self.sk7 = Connection(config.serial_port)
            connected = self.sk7.connect()

        if not connected:
            print('Unable to connect')
            sys.exit(-1)

        # create an instance of the fusion algorithm object for each IMU
        self.rtimus = [IMU(self.sk7, index - 1) for index in config.relative_imus]

        # raw angle
        self.angle = 0

        # scaled angle
        self.scaled = 32

        # calibration stuff
        if config.use_calibration:
            self.scale = (config.calibration[1] - config.calibration[0]) / 63.0
            self.offset = config.calibration[0]

    def close(self):
        for r in self.rtimus:
            r.close()

    def update(self):
        # update the state of each of the IMUs
        for r in self.rtimus:
            r.update()

        # retrieve the euler angles from the fusion algorithm
        s1 = self.rtimus[0].angles
        s2 = self.rtimus[1].angles

        # angle between the two devices along the axis we're interested in
        angle = (s1[0] - s2[0])

        # attempting to prevent sign flipping/wraparound problems 
        angle %= 360
        if angle > 180:
            angle = 360 - angle

        if s1[0] < s2[0]:
            angle *= -1

        self.angle = angle
        self.scaled = self.rescale(angle)

        # calibrated output if enabled
        if config.use_calibration:
            self.scaled = self.calibrate(self.scaled)

        print('E1: %.0f %.0f | E2: %.0f %.0f | E3: %.0f %.0f | DIFFS %.0f %.0f | %.3f %d | %d' % (s1[0], s2[0], s1[1], s2[1], s1[2], s2[2], s1[1] - s2[1], s1[0] - s2[0], angle, angle, self.scaled))

    def calibrate(self, val):
        # return output scaled based on config.calibration so that the entire
        # 1-64 range is used
        calibrated = 1 + ((val - self.offset) / self.scale)
        return int(max(1, min(calibrated, 64)))

    def rescale(self, angle):
        # scale the raw angle in degrees into the 1-64 range, assuming that the
        # min and max angles are -90/+90 degrees
        angle = max(-90, min(angle, 90))
        scaled = 32 + (angle * (64 / 180.0))
        return int(max(1, min(scaled, 64)))

def test():
    test = Test()
    running = True

    screen_width = 1000
    screen_height = 750
    rects = [pygame.Rect(0, i * (screen_height / 4), screen_width, screen_height / 4) for i in range(4)]

    a_bg = pygame.Color(22, 22, 22)
    sa_bg = pygame.Color(44, 44, 44)
    stream_colors = [pygame.Color(255, 0, 0), pygame.Color(0, 255, 0), pygame.Color(0, 0, 255), pygame.Color(255, 0, 255), pygame.Color(255, 255, 0)]

    
    pygame.init()
    screen = pygame.display.set_mode((screen_width, screen_height))

    a_graph = ScrollingGraph(a_bg, [stream_colors[0]], "Relative angle", rects[0], 250, -190, 190, 1, [-90, 0, 90])
    sa_graph = ScrollingGraph(sa_bg, [stream_colors[1]], "Scaled angle", rects[1], 250, 0, 65, 1, [1, 32, 64])
    i1_graph = ScrollingGraph(a_bg, [stream_colors[3]], "IMU 0 angle", rects[2], 250, -190, 190, 1, [-90, 0, 90])
    i2_graph = ScrollingGraph(a_bg, [stream_colors[4]], "IMU 1 angle", rects[3], 250, -190, 190, 1, [-90, 0, 90])

    try:
        while running:
            # update internal state of the IMUs
            test.update()

            event = pygame.event.poll()
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYUP:
                if event.key == K_ESCAPE:
                    running = False

            a_graph.draw(screen, [test.angle])
            sa_graph.draw(screen, [test.scaled])
            i1_graph.draw(screen, [test.rtimus[0].angles[0]])
            i2_graph.draw(screen, [test.rtimus[1].angles[0]])
    
            pygame.display.flip()
            time.sleep(0.01)
    except KeyboardInterrupt:
        pass

    print('Closing connection...')
    test.close()

def relative_callback(param):
    test = param[0]
    test.update()
    
    return [test.scaled]

def raw_callback(param):
    test = param[0]

    return [x.angles[0] for x in test.rtimus]

def start_pytia():
    test = Test()
    server = TiAServer((config.tia_server_ip, config.tia_server_port), TiAConnectionHandler)
    signals = []
    relative_signal = TiASignalConfig(
        channels=1, sample_rate=100, blocksize=1,
        callback=relative_callback, id=(test,), is_master=True, sigtype=pytia.TIA_SIG_USER_1)
    signals.append(relative_signal)

    if config.send_raw_angles:
        raw_signal = TiASignalConfig(
            channels=2, sample_rate=100, blocksize=1, 
            callback=raw_callback, id=(test,), is_master=False, sigtype=pytia.TIA_SIG_USER_2)
        signals.append(raw_signal)

    server.start(signals)
    print('[Ctrl-C to exit]')
    try:
        while True:
            time.sleep(0.1)
    except KeyboardInterrupt:
        print('Closing connection...')
    test.close()

if __name__ == "__main__":
    if "test" in sys.argv:
        if have_pygame:
            test()
        else:
            print('Pygame is required for the visualization to work')
    else:
        start_pytia()

