# address of the USB serial port for the IMUs. If you provide a list it will
# try each in turn until one works or the list is exhausted.
serial_port = ['COM7:', 'COM5:']

# IP/hostname to bind the TiA server (empty string means use all local interfaces)
tia_server_ip      = ''

# port number that the TiA server will run on (change this if it clashes with
# the TOBI signal server!)
tia_server_port    = 9000

# indices of the 2 IMUs to use for relative tilt calculations:
#   IMU 1 = sensor closest to the SHAKE
#   IMU 5 = sensor furthest from the SHAKE (end of the cable)
relative_imus      = [1, 3]

# also send raw angles from each IMU
send_raw_angles = True

# if set to True, output calibrated values based on the min/max values
# given in the "calibration" setting. If set to False then raw values
# are output (where 1=-90 degrees and 64=+90 degrees)
use_calibration = True

# if you want to ensure you get the full range of 1-64 in the output
# then measure the min/max outputs with use_calibration set to False,
# then insert those values into the following list and set use_calibration
# to True. After this, the raw output will be scaled into the 1-64 range
# (any values falling outside the range will be clamped to the nearesst 
# limit to prevent incorrect values being output)
calibration = [22, 44]
