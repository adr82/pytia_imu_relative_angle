import sys
from pytia import TiAClient

if __name__ == "__main__":
    server_address = '127.0.0.1'
    client = TiAClient(server_address, 9000)
    if not client.connect():
        print('Connection failed')
        sys.exit(-1)

    status, dport = client.cmd_get_data_connection_tcp()
    if not status:
        client.disconnect()
        print('Data connection failed')
        sys.exit(-1)

    if not client.start_streaming_data_tcp(server_address, dport):
        client.disconnect()
        print('Data streaming failed')
        sys.exit(-1)

    import time

    try:
        while True:
            packets = client.get_data()

            for p in packets:
                data = p.get_channel(0)
                print(data)

    except Exception as e:
        print(e)

    # disconnect from the server when done streaming
    client.stop_streaming_data()
    client.disconnect()
