# TODO find out why this crashes if not imported first...!
import RTIMU

import time, sys, math

visualizer_available = True
try:
    import pygame
    from pygame.locals import *
    from OpenGL.GL import *
    from OpenGL.GLU import *
except:
    visualizer_available = False

from importlib import import_module

from pyshake import *

def deg2rad(degs):
    return (math.pi / 180.0) * degs

def rad2deg(rads):
    return (rads * 180.0) / math.pi

class Connection(object):

    def __init__(self, addr):
        self.addr = addr
        self.sd = None

    def connect(self, addr=None):
        if addr is not None:
            self.addr = addr

        connected = False
        self.sd = ShakeDevice(SHAKE_SK7)
        try:
            print('Connecting to %s...' % self.addr)
            connected = self.sd.connect(self.addr)
        except Exception as e:
            print(e)
    
        if not connected:
            print('Failed to connect to "%s"' % self.addr)
            self.sd = None

        return connected

    def disconnect(self):
        if self.sd is None:
            return

        self.sd.close()

class BaseSK7(object):

    def __init__(self, dev, calibration=True):
        self.dev = dev

        self.acc_enabled = True
        self.mag_enabled = True
        self.gyro_enabled = True

        self.calibration = calibration
        if self.calibration:
            self.load_calibration()

        self.angles = [0, 0, 0]
        self.quaternion = [0, 0, 0, 0]

        self.print_sensors = False
        self.print_angles = False
        self.timestamp = time.time()

        self.imu = RTIMU.RTIMU(RTIMU.Settings('sk7rtimu'))
        self.imu.IMUInit()

    def calibrate(self):
        # don't want to return calibrated data for the moment
        calib_state = self.calibration
        self.calibration = False

        gyrobiases = [0, 0, 0]
        print('Gyro bias calibration will start in 2 seconds...')
        time.sleep(2.0)
    
        gyro_time = 3
        start_time = time.time()
        totals = [0, 0, 0] 
        count = 0
        progress = 0
        print('Calculating gyro biases for %ds' % gyro_time)
        while time.time() - start_time < gyro_time:
            if int(time.time() - start_time) > progress:
                progress += 1
                print('  %ds left' %  (gyro_time - progress))

            gyro = self.raw_gyro_fn()
            totals[0] += gyro[0]
            totals[1] += gyro[1]
            totals[2] += gyro[2]

            count += 1
            time.sleep(0.005)
        print('Completed!')
        gyrobiases = [int(t / (count * 1.0)) for t in totals]

        print('\nMagnetometer calibration started, press Ctrl-C to finish')

        mins = [1e06, 1e06, 1e06]
        maxs = [-1e06, -1e06, -1e06]

        try:
            while True:
                mag = self.mag_fn()

                for ax in range(3):
                    if mag[ax] < mins[ax]:
                        mins[ax] = mag[ax]
                    if mag[ax] > maxs[ax]:
                        maxs[ax] = mag[ax]

                time.sleep(0.005)
        except KeyboardInterrupt:
            pass

        print('Completed!')
        magbiases = [int((maxs[i] + mins[i]) / 2.0) for i in range(3)]
        print magbiases
        magscalings = [(maxs[i] - mins[i]) / 2.0 for i in range(3)]
        print magscalings
        avg_rads = sum(magscalings) / 3.0
        print avg_rads
        magscalings = [avg_rads / magscalings[i] for i in range(3)]

        self.calibration = calib_state
        return (gyrobiases, magbiases, magscalings)


    def update(self):
        """
        Updates sensor fusion state at each time step.
        Expects gyro readings in rad/s, timestamp microseconds (seems to be
        a cumulative time required, not the time since last call).
        """
        (ax, ay, az) = self.acc_fn()
        (gx, gy, gz) = self.gyro_fn() 
        (mx, my, mz) = self.mag_fn()

        if self.print_sensors:
            print(ax, ay, az, gx, gy, gz, mx, my, mz)

        # pass new data to the fusion algorith and then read back the new state
        self.imu.setExtIMUData(gx, gy, gz, ax, ay, az, mx, my, mz, int((time.time() - self.timestamp) * 1e6))
        data = self.imu.getIMUData()
        if self.print_angles:
            fusionPose = data["fusionPose"] 
            print("r: %f p: %f y: %f" % (math.degrees(fusionPose[0]), math.degrees(fusionPose[1]), math.degrees(fusionPose[2])))

        self.angles, self.quaternion = map(math.degrees, data['fusionPose']), data['fusionQPose']

        return self.angles, self.quaternion

    def calibrated_mag(self, rawxyz, xyzbias, scaling):
        return [(rawxyz[i] - xyzbias[i]) * scaling[i] for i in range(3)]

    def calibrated_gyro(self, rawxyz, xyzbias):
        return [(rawxyz[i] - xyzbias[i]) for i in range(3)]

    def toggle_acc(self):
        self.acc_enabled = not self.acc_enabled
        self.imu.setAccelEnable(self.acc_enabled)
        return self.acc_enabled

    def toggle_mag(self):
        self.mag_enabled = not self.mag_enabled
        self.imu.setCompassEnable(self.mag_enabled)
        return self.mag_enabled

    def toggle_gyro(self):
        self.gyro_enabled = not self.gyro_enabled
        self.imu.setGyroEnable(self.gyro_enabled)
        return self.gyro_enabled

    def reset(self):
        self.timestamp = time.time()
        self.angles = [0, 0, 0]
        self.quaternion = [0, 0, 0, 0]
        self.imu.resetFusion()

    def close(self):
        self.dev.disconnect()
    
    def load_calibration(self):
        try:
            mod = import_module(self.CALIB_NAME)
            device_data = mod.calibration_data[self.addr]
            self.gyrobiases = device_data['gyrobiases']
            self.magbiases = device_data['magbiases']
            self.magscalings = device_data['magscalings']
            print('Loaded calibration data for %s' % self.addr)
            self.calibration = True
        except Exception:
            self.gyrobiases, self.magbiases, self.magscaling = None, None, None
            print('Warning: no calibration available for device %s!' % self.addr)
            self.calibration = False

    # data access functions to be implemented by subclasses

    def acc_fn(self):
        pass

    def mag_fn(self):
        pass

    def gyro_fn(self):
        pass

class SK7(BaseSK7):

    CALIB_NAME = 'sk7_calibration'

    def __init__(self, dev, calibration=True):
        self.addr = dev.addr
        BaseSK7.__init__(self, dev)
        print('SK7 connected, writing config')
        self.dev.sd.write(0x0005, 0x07) # use user calibration coefficients for all sensors
        self.dev.sd.write_sample_rate(SHAKE_SENSOR_ACC, 100)
        self.dev.sd.write_sample_rate(SHAKE_SENSOR_MAG, 100)
        self.dev.sd.write_sample_rate(SHAKE_SENSOR_GYRO, 100)
        self.dev.sd.write_data_format(0x02)
        print('Config written')

    def acc_fn(self):
        return self.dev.sd.acc()

    def raw_gyro_fn(self):
        return self.dev.sd.gyro()

    def gyro_fn(self):
        gyro = self.dev.sd.gyro()

        if self.calibration:
            gyro = self.calibrated_gyro(gyro, self.gyrobiases)

        # tenths of degrees/sec to rad/s
        gyro = [deg2rad(g / 10.0) for g in gyro]

        # also swap x/y axes as required for SK7s
        gyro = [gyro[1], gyro[0], gyro[2]]

        return gyro

    def mag_fn(self):
        mag = self.dev.sd.mag()

        if self.calibration:
            mag = self.calibrated_mag(mag, self.magbiases, self.magscalings)

        return mag

    def calibrate(self):
        gyrobiases, magbiases, magscalings = BaseSK7.calibrate(self)

        try:
            mod = import_module(SK7.CALIB_NAME)
            calibration_data = mod.calibration_data
        except:
            calibration_data = {}

        data = {}
        data['gyrobiases'] = gyrobiases
        data['magbiases'] = magbiases
        data['magscalings'] = magscalings
        calibration_data[self.addr] = data

        with open('%s.py' % SK7.CALIB_NAME, 'w') as f:
            f.write('calibration_data = {\n')
            for device in calibration_data:
                data = calibration_data[device]
                f.write('\t"%s": { "gyrobiases" : %s, "magbiases": %s, "magscalings": %s },\n' % (device, gyrobiases, magbiases, magscalings))
            f.write('}\n')

        print('Calibration for %s written to %s.py' % (self.addr, SK7.CALIB_NAME))

class IMU(BaseSK7):

    CALIB_NAME = 'sk7_imu_calibration'

    def __init__(self, dev, imu_index, calibration=True):
        self.addr = '%s_%d' % (dev.addr, imu_index)
        BaseSK7.__init__(self, dev)
        self.index = imu_index

    @staticmethod
    def _fix_axes(xyz):
        """
        Remaps the sensor axes to conform to the SK7 layout. This ensures that the
        "roll" output from the algorithm (which has range -180 -- 180 degrees) will
        be tracking the axis the IMUs will be being rotated around. Otherwise they
        will use the "pitch" output which for this algorithm has range -90 -- 90
        degrees and doesn't seem to work as well for the current demo.
        """
        return [xyz[1], -xyz[0], xyz[2]]

    def acc_fn(self):
        return IMU._fix_axes(self.dev.sd.sk7_imu(self.index).acc)

    def raw_gyro_fn(self):
        return self.dev.sd.sk7_imu(self.index).gyro

    def gyro_fn(self):
        gyro = self.dev.sd.sk7_imu(self.index).gyro

        if self.calibration:
            gyro = self.calibrated_gyro(gyro, self.gyrobiases)

        # tenths of degrees/sec to rad/s
        gyro = [deg2rad(g / 10.0) for g in gyro]

        return IMU._fix_axes(gyro)

    def mag_fn(self):
        mag = self.dev.sd.sk7_imu(self.index).mag

        if self.calibration:
            mag = self.calibrated_mag(mag, self.magbiases, self.magscalings)

        return IMU._fix_axes(mag)

    def calibrate(self):
        gyrobiases, magbiases, magscalings = BaseSK7.calibrate(self)

        try:
            mod = import_module(IMU.CALIB_NAME)
            calibration_data = mod.calibration_data
        except:
            calibration_data = {}

        data = {}
        data['gyrobiases'] = gyrobiases
        data['magbiases'] = magbiases
        data['magscalings'] = magscalings
        calibration_data[self.addr] = data

        with open('%s.py' % IMU.CALIB_NAME, 'w') as f:
            f.write('calibration_data = {\n')
            for device in calibration_data:
                data = calibration_data[device]
                f.write('\t"%s": { "gyrobiases" : %s, "magbiases": %s, "magscalings": %s },\n' % (device, data['gyrobiases'], data['magbiases'], data['magscalings']))
            f.write('}\n')

        print('Calibration for %s written to %s.py' % (self.addr, IMU.CALIB_NAME))

class RTIMUVisualizer(object):

    def __init__(self, dev, width=640, height=480):
        self.dev = dev
        self.width = width
        self.height = height

    def resize(self, width, height):
        if height == 0:
            height = 1
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 1.0 * width / height, 0.1, 100.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def draw(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glLoadIdentity()
        glTranslatef(0, 0.0, -10.0)

        self.drawquad(self.dev.angles, (0, 0, 0))

    def drawquad(self, angles, t):
        glPushMatrix()
        glTranslatef(*t)

        angles = [angles[1], angles[0], -angles[2]]
        # note YZX order of rotations!
        glRotatef(angles[2], 0, 1, 0) # y-axis = yaw
        glRotatef(-angles[1], 0, 0, 1) # z-axis = pitch
        glRotatef(angles[0], 1, 0, 0) # x-axis = roll

        glBegin(GL_QUADS)
        glColor3f(0.0, 1.0, 0.0)
        glVertex3f(1.0, 0.2, -1.0)
        glVertex3f(-1.0, 0.2, -1.0)
        glVertex3f(-1.0, 0.2, 1.0)
        glVertex3f(1.0, 0.2, 1.0)

        glColor3f(1.0, 0.5, 0.0)
        glVertex3f(1.0, -0.2, 1.0)
        glVertex3f(-1.0, -0.2, 1.0)
        glVertex3f(-1.0, -0.2, -1.0)
        glVertex3f(1.0, -0.2, -1.0)

        glColor3f(1.0, 0.0, 0.0)
        glVertex3f(1.0, 0.2, 1.0)
        glVertex3f(-1.0, 0.2, 1.0)
        glVertex3f(-1.0, -0.2, 1.0)
        glVertex3f(1.0, -0.2, 1.0)

        glColor3f(1.0, 1.0, 0.0)
        glVertex3f(1.0, -0.2, -1.0)
        glVertex3f(-1.0, -0.2, -1.0)
        glVertex3f(-1.0, 0.2, -1.0)
        glVertex3f(1.0, 0.2, -1.0)

        glColor3f(0.0, 0.0, 1.0)
        glVertex3f(-1.0, 0.2, 1.0)
        glVertex3f(-1.0, 0.2, -1.0)
        glVertex3f(-1.0, -0.2, -1.0)
        glVertex3f(-1.0, -0.2, 1.0)

        glColor3f(1.0, 0.0, 1.0)
        glVertex3f(1.0, 0.2, -1.0)
        glVertex3f(1.0, 0.2, 1.0)
        glVertex3f(1.0, -0.2, 1.0)
        glVertex3f(1.0, -0.2, -1.0)
        glEnd()

        glPopMatrix()

    def init(self):
        glShadeModel(GL_SMOOTH)
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClearDepth(1.0)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

    def drawText(self, position, textString):
        font = pygame.font.SysFont("Courier", 18, True)
        textSurface = font.render(textString, True, (255, 255, 255, 255), (0, 0, 0, 255))
        textData = pygame.image.tostring(textSurface, "RGBA", True)
        glRasterPos3d(*position)
        glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)

    def main(self):
        video_flags = OPENGL | DOUBLEBUF
        pygame.init()
        pygame.display.set_mode((self.width, self.height), video_flags)
        pygame.display.set_caption("Press Esc to quit")
        self.resize(self.width, self.height)
        self.init()
        clock = pygame.time.Clock()

        while 1:
            try:
                event = pygame.event.poll()
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    break
                if event.type == KEYDOWN:
                    if event.key == K_v:
                        self.dev.print_sensors = not self.dev.print_sensors
                    elif event.key == K_r:
                        print('reset!')
                        self.dev.reset()
                    elif event.key == K_c:
                        self.dev.calibration = not self.dev.calibration
                        print('Calibration active?: %s' % (self.dev.calibration))
                    elif event.key == K_g:
                        self.dev.toggle_gyro()
                    elif event.key == K_a:
                        self.dev.toggle_acc()
                    elif event.key == K_m:
                        self.dev.toggle_mag()
                    elif event.key == K_e:
                        self.dev.print_angles = not self.dev.print_angles

                self.dev.update()
                self.draw()

                clock.tick(100)
                pygame.display.flip()
            except KeyboardInterrupt:
                break


if __name__ == '__main__': 
    if len(sys.argv) < 2:
        print('Missing device address')
        sys.exit(0)


    param = sys.argv[1]
    conn = Connection(param)
    if not conn.connect():
        sys.exit(-1)

    # if param.find('FTR') != -1:
    #     dev = IMU(conn, 0)
    # else:
    #     dev = SK7(conn)

    dev = IMU(conn, 2)
    # dev = SK7(conn)

    if "calibrate" in sys.argv:
        dev.calibrate()

    RTIMUVisualizer(dev).main()
